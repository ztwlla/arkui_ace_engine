/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "core/components_ng/render/sk_painter.h"

#include "include/core/SkColor.h"
#include "include/core/SkPaint.h"
#include "include/core/SkPath.h"
#include "include/effects/SkDashPathEffect.h"
#include "include/utils/SkParsePath.h"

#include "core/components/common/layout/constants.h"
#include "core/components_ng/pattern/shape/path_paint_property.h"
#include "core/components_ng/render/paint.h"

namespace OHOS::Ace::NG {
void SkPainter::DrawPath(RSCanvas& canvas, const std::string& commands, const ShapePaintProperty& shapePaintProperty)
{
    auto rsCanvas = canvas.GetImpl<RSSkCanvas>();
    if (!rsCanvas) {
        return;
    }
    auto skCanvas = rsCanvas->ExportSkCanvas();
    if (!skCanvas) {
        return;
    }
    SkPaint skPaint;
    SkPath skPath;
    bool ret = SkParsePath::FromSVGString(commands.c_str(), &skPath);
    if (!ret) {
        return;
    }
    SetPaint(skPaint, shapePaintProperty);
    skCanvas->drawPath(skPath, skPaint);
}

void SkPainter::SetPaint(SkPaint& skPaint, const ShapePaintProperty& shapePaintProperty)
{
    skPaint.setStyle(SkPaint::Style::kStroke_Style);
    if (shapePaintProperty.HasAntiAlias()) {
        skPaint.setAntiAlias(shapePaintProperty.GetAntiAliasValue());
    } else {
        skPaint.setAntiAlias(shapePaintProperty.ANTIALIAS_DEFAULT);
    }

    if (shapePaintProperty.HasStrokeLineCap()) {
        int lineCap = shapePaintProperty.GetStrokeLineCapValue();
        if (static_cast<int>(LineCapStyle::ROUND) == lineCap) {
            skPaint.setStrokeCap(SkPaint::Cap::kRound_Cap);
        } else if (static_cast<int>(LineCapStyle::SQUARE) == lineCap) {
            skPaint.setStrokeCap(SkPaint::Cap::kSquare_Cap);
        } else {
            skPaint.setStrokeCap(SkPaint::Cap::kButt_Cap);
        }
    } else {
        skPaint.setStrokeCap(SkPaint::Cap::kButt_Cap);
    }

    if (shapePaintProperty.HasStrokeLineJoin()) {
        int lineJoin = shapePaintProperty.GetStrokeLineJoinValue();
        if (static_cast<int>(LineJoinStyle::ROUND) == lineJoin) {
            skPaint.setStrokeJoin(SkPaint::Join::kRound_Join);
        } else if (static_cast<int>(LineJoinStyle::BEVEL) == lineJoin) {
            skPaint.setStrokeJoin(SkPaint::Join::kBevel_Join);
        } else {
            skPaint.setStrokeJoin(SkPaint::Join::kMiter_Join);
        }
    } else {
        skPaint.setStrokeJoin(SkPaint::Join::kMiter_Join);
    }

    if (shapePaintProperty.HasStrokeWidth()) {
        skPaint.setStrokeWidth(static_cast<SkScalar>(shapePaintProperty.GetStrokeWidthValue().ConvertToPx()));
    } else {
        skPaint.setStrokeWidth(static_cast<SkScalar>(shapePaintProperty.STOKE_WIDTH_DEFAULT.ConvertToPx()));
    }

    if (shapePaintProperty.HasStroke()) {
        skPaint.setStyle(SkPaint::Style::kStroke_Style);
        Color strokeColor = shapePaintProperty.GetStrokeValue();
        double curOpacity = shapePaintProperty.STOKE_OPACITY_DEFAULT;
        if (shapePaintProperty.HasStrokeOpacity()) {
            curOpacity = shapePaintProperty.GetStrokeOpacityValue();
        }
        skPaint.setColor(strokeColor.BlendOpacity(curOpacity).GetValue());
    } else {
        skPaint.setColor(Color::BLACK.GetValue());
    }

    if (shapePaintProperty.HasStrokeDashArray()) {
        auto lineDashState = shapePaintProperty.GetStrokeDashArrayValue();
        SkScalar intervals[lineDashState.size()];
        for (size_t i = 0; i < lineDashState.size(); ++i) {
            intervals[i] = static_cast<SkScalar>(lineDashState[i].ConvertToPx());
        }
        SkScalar phase = 0.0f;
        if (shapePaintProperty.HasStrokeDashOffset()) {
            phase = static_cast<SkScalar>(shapePaintProperty.GetStrokeDashOffsetValue().ConvertToPx());
        }
        skPaint.setPathEffect(SkDashPathEffect::Make(intervals, lineDashState.size(), phase));
    }

    if (shapePaintProperty.HasStrokeMiterLimit()) {
        skPaint.setStrokeMiter(static_cast<SkScalar>(shapePaintProperty.GetStrokeMiterLimitValue()));
    }

    if (shapePaintProperty.HasFill()) {
        skPaint.setStyle(SkPaint::Style::kFill_Style);
        Color fillColor = shapePaintProperty.GetFillValue();
        double curOpacity = shapePaintProperty.STOKE_OPACITY_DEFAULT;
        if (shapePaintProperty.HasStrokeOpacity()) {
            curOpacity = shapePaintProperty.GetStrokeOpacityValue();
        }
        skPaint.setColor(fillColor.BlendOpacity(curOpacity).GetValue());
    } else {
        skPaint.setColor(Color::BLACK.GetValue());
    }
}
} // namespace OHOS::Ace::NG