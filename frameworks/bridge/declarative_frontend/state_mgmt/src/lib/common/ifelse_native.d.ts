
// bindings defined in js_ifelse.cpp/h
declare class If  {
    static create(): void;
    static pop() : void;
    static branchId(id: number): void;
    static getBranchId() : number;
}
